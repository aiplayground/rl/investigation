import gym
import numpy as np
import matplotlib.pyplot as plt

keep_ids = np.array([2, 8, 11, 12, 18, 21, 49, 51, 54], dtype=np.uint8)

def show_image(x, sp=None):
    plt.imshow(x, cmap='gray')
    plt.xticks([]); plt.yticks([]); plt.grid(False)
    if sp is not None: plt.savefig(sp)
    plt.show()

def crop_observation(o):
    return o[34:194].mean(-1).astype(np.uint8)

def find_paddle(o, left):
    if left: idx = 16, 139
    else: idx = 140, 123

    try: return np.where(o[:, idx[0]] == idx[1])[0][0]
    except IndexError: return None

def find_paddles(o):
    return find_paddle(o, left=True), find_paddle(o, left=False)

def find_ball(o):
    idx = np.where(o == 236)
    try: return idx[1][0], idx[0][0]
    except IndexError: return None, None

def get_state(o):
    if o is None: return None

    return (*find_paddles(o), *find_ball(o))

def get_velocities(o_p, o_n):
    l_p, r_p, b_px, b_py = o_p
    l_n, r_n, b_nx, b_ny = o_n

    if l_p is None or l_n is None: l_v = 0.
    else: l_v = l_n - l_p
    if r_p is None or r_n is None: r_v = 0.
    else: r_v = r_n - r_p

    if b_px is None or b_nx is None: b_vx = b_vy = 0.
    else: b_vx = b_nx - b_px; b_vy = b_ny - b_py

    return l_v, r_v, b_vx, b_vy

def standardize_observation(o, env):
    for idx in range(4):
        if o[idx * 2] is None:
            if env.prev_o[idx] is not None:
                o[idx * 2] = env.prev_o[idx]
            else:
                o[idx * 2] = 80.

def normalize_observation(o):
    return o.astype(np.float32) / 160.

def denormalize_observation(o):
    return (o * 160).astype(np.uint8)

def preprocess_observation(o, env):
    o = crop_observation(o)
    o = get_state(o)
    v = get_velocities(env.prev_o, o)
    env.prev_o = o
    o = np.array([o[0], v[0], o[1], v[1], o[2], v[2], o[3], v[3]])
    standardize_observation(o, env)
    return normalize_observation(o)

def get_raw(o):
    l, r, b_x, b_y = denormalize_observation(o[::2])

    raw_o = np.ones((160, 160), dtype=np.uint8) * 77

    raw_o[l:l + 16, 16:20] = 139
    raw_o[r:r + 16, 140:144] = 123
    raw_o[b_y: b_y + 4, b_x: b_x + 2] = 236

    return raw_o

def make_env(xtreme=False):
    from types import MethodType
    env = gym.make('PongDeterministic-v4')

    _reset = env.reset
    def reset(self):
        self.prev_o = [None] * 4
        o = preprocess_observation(_reset(), env)
        if xtreme: o = np.array([o[6] - o[2]])
        return o
    env.reset = MethodType(reset, env)

    _step = env.step
    def step(self, action):
        o, r, d, i = _step(action + 2)
        o = preprocess_observation(o, self)
        if xtreme: o = np.array([o[6] - o[2]])
        return o, r, d, i
    env.step = MethodType(step, env)

    env.n = 8 if not xtreme else 1
    env.a = 2

    return env

def make_env_ram():
    from types import MethodType
    env = gym.make('Pong-ramDeterministic-v4')

    _step = env.step
    def step(self, action):
        return _step(action + 2)
    env.step = MethodType(step, env)

    env.action_space.n = 2

    return env
